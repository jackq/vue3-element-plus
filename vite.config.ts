/*
 * @Description:
 * @Date: 2021-10-22 09:39:41
 * @LastEditTime: 2022-05-06 17:17:22
 * @FilePath: /vue3-element-plus/vite.config.ts
 * @Author: Devin
 */
import { defineConfig, searchForWorkspaceRoot } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
export default defineConfig({
  plugins: [
      
    vue(),
   
  ],
  server: {
    port: 8080, 
    open: false,
    host: '0.0.0.0',
    fs: {
      allow: [
        // search up for workspace root
        searchForWorkspaceRoot(process.cwd()),
        // your custom rules
        '/input/list',
        '/select/list'
      ]
    }
  },
  build: {
    outDir: 'dist'
  },
  base: '',
  resolve: {
    alias: [
      {
        find: '@',
        replacement: resolve('src') 
      }
    ]
  }
});
