# vue3.0+element-plus

#### 介绍
基于vue3.0和element-plus的后台管理模板，并对element-plus二次封装
#### 预览地址：
http://devincp.gitee.io/vue3-element-plus

![img](https://gitee.com/DevinCP/vue3-element-plus/raw/master/src/assets/img/login.png)

![img](https://gitee.com/DevinCP/vue3-element-plus/raw/master/src/assets/img/table.png)

![img](https://gitee.com/DevinCP/vue3-element-plus/raw/master/src/assets/img/them.png)
