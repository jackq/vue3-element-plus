/*
 * @Description:
 * @Author: Devin-chen
 * @Date: 2021-10-24 15:18:08
 * @LastEditTime: 2021-10-24 15:32:58
 * @LastEditors: Devin-chen
 * @Reference: 原创
 */

import { createI18n, I18n } from 'vue-i18n';
import zh from './lang/zh';
import en from './lang/en';
const messages = { zh, en };

// 通过选项创建 VueI18n 实例

const i18n: I18n<
  {
    zh: any;
    en: any;
  },
  unknown,
  unknown,
  true
> = createI18n({
  locale: 'zh', // 设置地区
  messages // 设置地区信息
});

export default i18n;
