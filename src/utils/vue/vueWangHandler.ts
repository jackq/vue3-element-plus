/*
 * @Description:
 * @Date: 2021-10-22 13:20:49
 * @LastEditTime: 2022-02-08 09:59:32
 * @FilePath: /vue3-element-plus/src/utils/vue/vueWangHandler.ts
 * @Author: Devin
 */
export function configWarnHandlerFun(app: any): void {
	app.config.warnHandler = function (msg: any, vm: any, trace: any) {
		// `trace` 是组件的继承关系追踪
    // console.warn(msg);
    // console.log(vm);
    // console.log(trace);
	};
}
