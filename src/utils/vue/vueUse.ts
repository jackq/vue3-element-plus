/*
 * @Description:
 * @Date: 2021-10-22 15:45:45
 * @LastEditTime: 2022-05-06 14:06:44
 * @FilePath: /vue3-element-plus/src/utils/vue/vueUse.ts
 * @Author: Devin
 */
import ccMessage from '@/components/packages/cc-message';
import i18n from '@/i18n';
import router from '@/router';
import store from '@/store';
import ElementPlus from 'element-plus';
import CcElementPlus from '@/components/packages/index';
import { Common } from '@/components/index';
import zhCn from 'element-plus/es/locale/lang/zh-cn';
import CcLoading from '@/components/loading';
import '@/components/packages/cc-icon-button/src/font/iconfont.css';
export function vueUseFun(app: any): void {
  app.use(router);
  app.use(ElementPlus, { size: 'small', locale: zhCn });
  app.use(store);
  app.use(i18n);
  app.use(Common);
  app.use(CcElementPlus);
  app.use(ccMessage);
  app.use(CcLoading);
}
