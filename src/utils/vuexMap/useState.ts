/*
 * @Description:
 * @Author: Devin
 * @Date: 2021-11-06 15:40:35
 * @LastEditTime: 2021-11-06 17:13:31
 * @LastEditors: Devin
 * @Reference:
 */
import { mapState, createNamespacedHelpers } from 'vuex';

import { useStateMapper } from './useMapper';

/**
 * @description 封装vuex的mapActions，以便在setup中使用
 * @param {*} moduleName 模块名称
 * @param {*} mapper getters属性集合 ['name', 'age']
 * @return {*}
 */
export function useState({
  moduleName,
  mapper
}: {
  moduleName?: string;
  mapper: string | string[];
}): any {
  let mapperFn = mapState;
  if (moduleName) {
    // 如果使用模块化，则使用vuex提供的createNamespacedHelpers方法找到对应模块的mapState方法
    if (Object.prototype.toString.call(moduleName) === '[object String]' && moduleName.length > 0) {
      mapperFn = createNamespacedHelpers(moduleName).mapState;
    }
  }

  return useStateMapper(mapper, mapperFn);
}
