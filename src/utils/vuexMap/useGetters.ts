/*
 * @Description:
 * @Author: Devin
 * @Date: 2021-11-06 15:52:31
 * @LastEditTime: 2021-11-06 17:13:36
 * @LastEditors: Devin
 * @Reference:
 */
import { mapGetters, createNamespacedHelpers } from 'vuex';

import { useStateMapper } from './useMapper';

/**
 * @description 封装vuex的mapGetters，以便在setup中使用
 * @param {*} moduleName 模块名称
 * @param {*} mapper getters属性集合 ['name', 'age']
 * @return {*}
 */
export function useGetters({
  moduleName,
  mapper
}: {
  moduleName?: string;
  mapper: string | string[];
}): any {
  let mapperFn = mapGetters;

  // 如果使用模块化，则使用vuex提供的createNamespacedHelpers方法找到对应模块的mapGetters方法
  if (moduleName) {
    if (Object.prototype.toString.call(moduleName) === '[object String]' && moduleName.length > 0) {
      mapperFn = createNamespacedHelpers(moduleName).mapGetters;
    }
  }

  return useStateMapper(mapper, mapperFn);
}
