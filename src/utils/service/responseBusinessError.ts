import { ccMessageConfirm } from "@/components/packages/cc-message-box";

/*
 * @Description:
 * @Date: 2021-09-18 14:33:29
 * @LastEditTime: 2022-02-17 15:32:35
 * @FilePath: /vue3-element-plus/src/utils/axios/responseBusinessError.ts
 * @Author: Devin
 */

function responseBusinessError(responseData: { code: number; message: any; }) {
  if (responseData.code === 50008 || responseData.code === 50012 || responseData.code === 50014) {
    // 去重新登录
    ccMessageConfirm({
      title: '确认注销',
      message: '您已注销，您可以取消停留在此页面，或重新登录',
      confirmButtonText: '重新登录',
      cancelButtonText: '取消',
    }, () => { console.log(responseData); })
  }
  return Promise.reject(new Error(responseData.message || 'Error'));
}
export default responseBusinessError