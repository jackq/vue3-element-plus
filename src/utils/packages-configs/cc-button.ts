/*
 * @Description: 
 * @Date: 2022-01-25 18:07:57
 * @LastEditTime: 2022-01-26 11:43:17
 * @FilePath: /vue3-element-plus/src/utils/packages-configs/cc-button.ts
 * @Author: Devin
 */

interface ccButton {
  size: string,
  autoInsertSpace: boolean,
  type: string
}

let config: ccButton = {
  size: 'default',
  autoInsertSpace: true,
  type: 'primary'
}

export default config