/*
 * @Description:
 * @Date: 2022-02-16 10:14:28
 * @LastEditTime: 2022-04-22 15:02:35
 * @FilePath: /vue3-element-plus/src/utils/packages-configs/cc-autocomplete.ts
 * @Author: Devin
 */
interface InputConfig {
  maxlength: number | string;
  minlength: number;
  showWordLimit: boolean;
  placeholder: string;
  clearable: boolean;
  size: string;
  autocompleteType: string;
  autocompleteList: any[];
  emailAutocompleteList: any[];
  valueKey: string;
  listField: string;
  caseSensitive: boolean;
  listType: string;
  url: string;
  params: object;
}

let config: InputConfig = {
  maxlength: 100,
  minlength: 0,
  showWordLimit: true,
  placeholder: '请输入',
  clearable: true,
  size: 'default',
  autocompleteType: '',
  autocompleteList: [],
  emailAutocompleteList: [
    { value: '@qq.com' },
    { value: '@136.com' },
    { value: '@126.com' },
    { value: '@sina.com' },
    { value: '@sohu.com' },
    { value: '@189.com' }
  ],
  valueKey: 'value',
  listField: 'data',
  caseSensitive: false,
  listType: 'local',
  url: '/input/list',
  params: {}
};
export default config;
