/*
 * @Description: 
 * @Date: 2022-02-08 11:32:03
 * @LastEditTime: 2022-02-10 23:21:51
 * @FilePath: /vue3-element-plus/src/utils/packages-configs/cc-textarea.ts
 * @Author: Devin
 */
interface InputConfig {
  maxlength: number | string,
  minlength: number,
  showWordLimit: boolean,
  placeholder: string,
  type: string,
  rows: number,
  resize: string,
  autosize: boolean | { minRows: number, maxRows: number }
}

let config: InputConfig = {
  maxlength: 500,
  minlength: 0,
  showWordLimit: true,
  placeholder: '请输入',
  type: 'textarea',
  rows: 2,
  resize: 'none',
  autosize: { minRows: 2, maxRows: 6 }
}
export default config