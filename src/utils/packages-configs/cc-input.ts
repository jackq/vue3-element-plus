/*
 * @Description: 
 * @Date: 2022-02-08 11:32:03
 * @LastEditTime: 2022-02-09 17:58:50
 * @FilePath: /vue3-element-plus/src/utils/packages-configs/cc-input.ts
 * @Author: Devin
 */
interface InputConfig {
  maxlength: number | string,
  minlength: number,
  showWordLimit: boolean,
  placeholder: string,
  clearable: boolean,
  size: string
}

let config: InputConfig = {
  maxlength: 100,
  minlength: 0,
  showWordLimit: true,
  placeholder: '请输入',
  clearable: true,
  size: 'default'
}
export default config