/*
 * @Author: Devin
 * @Date: 2022-03-18 15:44:58
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-18 15:47:58
 */

interface Config {
  size: string;
  optionsKey: object;
  options: any[];
}

const config: Config = {
  size: 'default',
  options: [],
  optionsKey: { value: 'value', label: 'label' }
};
export default config;
