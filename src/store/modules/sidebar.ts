/*
 * @Description:
 * @Author: Devin
 * @Date: 2021-11-06 15:06:41
 * @LastEditTime: 2022-01-26 11:41:56
 * @LastEditors: Please set LastEditors
 * @Reference:
 */
import { setThemeColor, storage } from '@/utils/tools';

function state() {
  return {
    sidebar: {
      closed: storage.getSession('sidebarStatus') == 1 ? true : false,
      activeTextColor: "var('--cc-main-color')",
      textColor: 'rgba(0,0,0,0.8)',
      backgroundColor: '#fff'
    }
  };
}

const mutations = {
  SWITCH_SIDEBAR: (state: { sidebar: { closed: boolean } }) => {
    state.sidebar.closed = !state.sidebar.closed;
    state.sidebar.closed
      ? storage.setSession('sidebarStatus', 1)
      : storage.setSession('sidebarStatus', 0);
  },
  CHANGE_MAIN_COLOR: (state: any, color: string) => {
    color = color.split('rgb(')[1].split(')')[0];
    setThemeColor(color);
  }
};

const getters = {};

interface actionsCommit {
  commit: any;
}

const actions = {
  switchSidebar({ commit }: actionsCommit) {
    commit('SWITCH_SIDEBAR');
  },
  changeMainColor({ commit }: actionsCommit, color: string,) {
    commit('CHANGE_MAIN_COLOR', color);
  }
};

export default {
  state,
  mutations,
  getters,
  actions
};
