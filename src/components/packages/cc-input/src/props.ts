/*
 * @Description: 
 * @Date: 2022-01-29 13:51:34
 * @LastEditTime: 2022-02-10 18:01:29
 * @FilePath: /vue3-element-plus/src/components/packages/cc-input/src/props.ts
 * @Author: Devin
 */
import config from "@/utils/packages-configs/cc-input"

const props = {
  modelValue: String,
  maxlength: {
    type: Number,
    default: config.maxlength
  },
  minlength: {
    type: Number,
    default: config.minlength
  },
  showWordLimit: {
    type: Boolean,
    default: config.showWordLimit
  },
  placeholder: {
    type: String,
    default: config.placeholder
  },
  clearable: {
    type: Boolean,
    default: config.clearable
  },
  size: {
    type: String,
    default: config.size
  }
}
export default props