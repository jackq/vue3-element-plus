/*
 * @Author: Devin
 * @Date: 2022-02-28 14:29:37
 * @LastEditors: Devin
 * @LastEditTime: 2022-05-06 17:11:11
 */
import CcButton from './cc-button';
import CcInput from './cc-input';
import CcTextarea from './cc-textarea';
import CcInputNumber from './cc-input-number';
import CcAutocomplete from './cc-autocomplete';
import CcDate from './cc-date';
import CcSelect from './cc-select';
import CcSelectV from './cc-select-v';
import CcForm from './cc-form';
import CcTable from './cc-table';
import CcAnchor from './cc-anchor';
import CcIconButton from './cc-icon-button';

export default {
  install(app: any, options: object = {}) {
    app.config.globalProperties.$CC = options;
    app.component(CcButton.name, CcButton);
    app.component(CcInput.name, CcInput);
    app.component(CcTextarea.name, CcTextarea);
    app.component(CcInputNumber.name, CcInputNumber);
    app.component(CcAutocomplete.name, CcAutocomplete);
    app.component(CcDate.name, CcDate);
    app.component(CcSelect.name, CcSelect);
    app.component(CcSelectV.name, CcSelectV);
    app.component(CcForm.name, CcForm);
    app.component(CcTable.name, CcTable);
    app.component(CcAnchor.name, CcAnchor);
    app.component(CcIconButton.name, CcIconButton);
  }
};
