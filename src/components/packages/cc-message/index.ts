/*
 * @Description: 二次封装element-plus的message提示，实现不重复提示
 * @Author: Devin-chen
 * @Date: 2021-10-22 23:04:23
 * @LastEditTime: 2022-04-01 17:05:10
 * @LastEditors: Devin
 * @Reference: 原创
 */

import { ElMessage, MessageHandle } from 'element-plus';
import config from '@/utils/packages-configs/cc-message';
let messageArray: MessageHandle[] = [];
export const isString = (o: any) => {
  return Object.prototype.toString.call(o).slice(8, -1) === 'String';
};
export interface CcMessage {
  config: {
    globalProperties: {
      $_message: {
        success: (message: any, number: number, options?: object | undefined) => void;
        error: (message: any, number: number, options?: object | undefined) => void;
        info: (message: any, number: number, options?: object | undefined) => void;
        warning: (message: any, number: number, options?: object | undefined) => void;
      };
    };
  };
}

function messageFun({ message, number = config.number, options }: MessageType, type: any) {
  if (messageArray.length >= number) {
    for (let index = 0; index <= messageArray.length - number; index++) {
      messageArray[index].close();
    }
    // 当message数组中的条数大于等于设定的数字时，需要删除最前面的数据
    messageArray.splice(0, messageArray.length - number + 1);
  }
  messageArray.push(
    ElMessage({
      message: message,
      type,
      ...options
    })
  );
}

export interface MessageType {
  message: any;
  number?: number;
  options?: object;
}

export const ccMessage = {
  success: (config: MessageType | string): void => {
    if (isString(config)) {
      messageFun({ message: config, number: 1, options: {} }, 'success');
    } else {
      messageFun(
        { message: (config as any).message, number: (config as any).number, options: (config as any).options },
        'success'
      );
    }
  },
  error: (config: MessageType | string): void => {
    if (isString(config)) {
      messageFun({ message: config, number: 1, options: {} }, 'error');
    } else {
      messageFun(
        { message: (config as any).message, number: (config as any).number, options: (config as any).options },
        'error'
      );
    }
  },
  info: (config: MessageType | string): void => {
    if (isString(config)) {
      messageFun({ message: config, number: 1, options: {} }, 'info');
    } else {
      messageFun(
        { message: (config as any).message, number: (config as any).number, options: (config as any).options },
        'info'
      );
    }
  },
  warning: (config: MessageType | string): void => {
    if (isString(config)) {
      messageFun({ message: config, number: 1, options: {} }, 'warning');
    } else {
      messageFun(
        { message: (config as any).message, number: (config as any).number, options: (config as any).options },
        'warning'
      );
    }
  }
};

export default {
  install: (app: CcMessage, options: any) => {
    app.config.globalProperties.$_message = ccMessage;
  }
};
