/*
 * @Author: Devin
 * @Date: 2022-03-17 15:04:47
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-18 14:54:56
 */
import Main from './src/main.vue';

Main.install = function (Vue: any) {
  Vue.component(Main.name, Main)
};

export default Main