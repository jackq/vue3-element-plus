/*
 * @Author: Devin
 * @Date: 2022-03-17 15:05:03
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-18 17:32:06
 */

import config from '@/utils/packages-configs/cc-select';
const props = {
  modelValue: [String, Number, Boolean, Object],
  options: {
    type: Array,
    default: () => config.options
  },
  optionsKey: {
    tyep: Object,
    default: () => config.optionsKey
  },
  size: {
    type: String,
    default: config.size
  },
  url: {
    // 请求下拉框URL地址，默认数据字典请求接口地址，非必传。
    type: String,
    default: ''
  },
  params: {
    type: Object
  },
  listField: {
    type: String,
    default: 'data'
  },
  dataHandler: Function,
  dataFilterHandler: Function
};

export default props;
