/*
 * @Author: Devin
 * @Date: 2022-04-11 17:47:34
 * @LastEditors: Devin
 * @LastEditTime: 2022-04-11 18:08:09
 */
import Main from './src/Main.vue';

Main.install = function (Vue: any) {
  Vue.component(Main.name, Main)
};

export default Main