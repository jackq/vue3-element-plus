/*
 * @Description:
 * @Date: 2022-01-29 13:51:34
 * @LastEditTime: 2022-02-10 23:09:50
 * @FilePath: /vue3-element-plus/src/components/packages/cc-textarea/src/props.ts
 * @Author: Devin
 */
import config from '@/utils/packages-configs/cc-textarea';

const props = {
  modelValue: String,
  maxlength: {
    type: Number,
    default: config.maxlength
  },
  minlength: {
    type: Number,
    default: config.minlength
  },
  showWordLimit: {
    type: Boolean,
    default: config.showWordLimit
  },
  placeholder: {
    type: String,
    default: config.placeholder
  },
  type: {
    type: String,
    default: config.type
  },
  rows: {
    type: Number,
    default: config.rows
  },
  resize: {
    type: String,
    default: config.resize
  },
  autosize: {
    type: [Boolean, Object],
    default: () => config.autosize
  }
};
export default props;
