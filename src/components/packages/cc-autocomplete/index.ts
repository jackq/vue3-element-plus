/*
 * @Description: 
 * @Date: 2022-01-29 13:51:13
 * @LastEditTime: 2022-03-17 18:06:06
 * @FilePath: /vue3-element-plus/src/components/packages/cc-autocomplete/index.ts
 * @Author: Devin
 */
import Main from './src/Main.vue';

Main.install = function (Vue: any) {
  Vue.component(Main.name, Main)
};

export default Main