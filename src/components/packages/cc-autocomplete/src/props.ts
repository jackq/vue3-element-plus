/*
 * @Description:
 * @Date: 2022-01-29 13:51:34
 * @LastEditTime: 2022-04-22 15:07:10
 * @FilePath: /vue3-element-plus/src/components/packages/cc-autocomplete/src/props.ts
 * @Author: Devin
 */
import config from '@/utils/packages-configs/cc-autocomplete';

const props = {
  modelValue: String,
  maxlength: {
    type: Number,
    default: config.maxlength
  },
  minlength: {
    type: Number,
    default: config.minlength
  },
  showWordLimit: {
    type: Boolean,
    default: config.showWordLimit
  },
  placeholder: {
    type: String,
    default: config.placeholder
  },
  clearable: {
    type: Boolean,
    default: config.clearable
  },
  size: {
    type: String,
    default: config.size
  },
  autocompleteType: {
    type: String,
    default: config.autocompleteType,
    validator: (value: string) => {
      const methodTypes = ['', 'email'];
      const valid = methodTypes.indexOf(value) !== -1;
      if (!valid) {
        throw new Error(`autocompleteType必须为['','email']其中一个`);
      }
      return valid;
    }
  },
  autocompleteList: {
    type: Array,
    default: () => config.autocompleteList
  },
  emailAutocompleteList: {
    type: Array,
    default: () => config.emailAutocompleteList
  },
  fetchSuggestions: Function,
  createFilter: Function,
  dataHandler: Function,
  valueKey: {
    type: String,
    default: config.valueKey
  },
  listField: { type: String, default: config.listField },
  caseSensitive: {
    type: Boolean,
    default: config.caseSensitive
  },
  listType: {
    type: String,
    default: config.listType,
    validator(value: string) {
      const types: Array<string> = ['remote', 'local'];
      const validType: boolean = types.indexOf(value) !== -1;
      if (!validType) {
        throw new Error(`Invalid listType of '${value}', please set one type of 'remote' or 'local'.`);
      }
      return validType;
    }
  },
  url: {
    type: String,
    default: config.url
  },
  params: {
    type: Object,
    default: () => config.params
  }
};
export default props;
