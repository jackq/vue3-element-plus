/*
 * @Author: Devin
 * @Date: 2022-03-21 15:36:52
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-21 15:37:54
 */
import Main from './src/main.vue';

Main.install = function (Vue: any) {
  Vue.component(Main.name, Main)
};

export default Main