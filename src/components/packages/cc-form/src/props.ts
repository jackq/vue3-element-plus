/*
 * @Author: Devin
 * @Date: 2022-03-21 15:37:03
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-30 18:04:16
 */

const props = {
  forms: Object,
  rowType: {
    type: Object,
    default: () => {
      return { gutter: 20, justify: 'start', align: 'top' };
    }
  },
  labelPosition: {
    type: String,
    default: 'top'
  },
  size: {
    type: String,
    default: 'default'
  },
  footerBtnPosition: {
    type: String,
    default: 'center'
  },
  isScrollToErrorField: {
    type: Boolean,
    default: true
  },
  showMessage: {
    type: Boolean,
    default: false
  },
  messageText: {
    type: String,
    default: '请检查校验项'
  }
};

export default props;
