/*
 * @Author: Devin
 * @Date: 2022-04-01 17:33:09
 * @LastEditors: Devin
 * @LastEditTime: 2022-04-27 18:18:22
 */
import config from '@/utils/packages-configs/cc-table';
import { h } from 'vue';
const EmptyType: Array<string> = ['str', 'img'];
export default {
  type: {
    type: String,
    default: config.type,
    validator(value: string) {
      const types = ['remote', 'local'];
      const validType = types.indexOf(value) !== -1;
      if (!validType) {
        throw new Error(`Invalid type of '${value}', please set one type of 'remote' or 'local'.`);
      }
      return validType;
    }
  },
  url: {
    type: String,
    default: config.url
  },
  method: {
    type: String,
    default: config.method,
    validator: (value: string) => {
      const methodTypes = ['get', 'post', 'put', 'delete'];
      return methodTypes.indexOf(value.toLowerCase()) !== -1;
    }
  },
  headers: {
    type: Object,
    default: () => {
      return config.headers;
    }
  },
  listField: {
    type: String,
    default: config.listField
  },
  totalField: {
    type: String,
    default: config.totalField
  },
  params: {
    type: Object,
    default: () => {
      return config.params;
    }
  },
  columns: {
    type: Array,
    required: true,
    default: () => config.columns
  },
  data: {
    type: Array,
    default: () => config.data
  },
  autoLoad: {
    type: Boolean,
    default: config.autoLoad,
    validator(value: boolean) {
      const types = [true, false];
      const validType = types.indexOf(value) !== -1;
      if (!validType) {
        throw new Error(`Invalid type of '${value}', please set one type of 'true' or 'false'.`);
      }
      return validType;
    }
  },
  showPagination: {
    type: Boolean,
    default: config.showPagination,
    validator(value: boolean) {
      const types = [true, false];
      const validType = types.indexOf(value) !== -1;
      if (!validType) {
        throw new Error(`Invalid type of '${value}', please set one type of 'true' or 'false'.`);
      }
      return validType;
    }
  },
  pageSizes: {
    type: Array,
    default: () => {
      return config.pageSizes;
    }
  },
  setPageIndex: {
    type: Number,
    default: config.setPageIndex
  },
  layout: {
    type: String,
    default: config.layout
  },
  pageIndexKey: {
    type: String,
    default: config.pageIndexKey
  },
  pageSizeKey: {
    type: String,
    default: config.pageSizeKey
  },
  background: {
    type: Boolean,
    default: config.background
  },
  paginationBind: Object,
  paginationOn: Object,
  paginationPosition: { type: [String, Array], default: 'center' },
  needLoading: {
    type: Boolean,
    default: config.needLoading
  },
  renderLoading: {
    type: Object,
    default: () => {
      return {
        render: () =>
          h('div', { class: 'cc-loading-container' }, [
            h('div', { class: 'cc-loading-first' }),
            h('div', { class: 'cc-loading-ball' }),
            h('div', { class: 'cc-loading-ball' }),
            h('div', { class: 'cc-loading-ball' }),
            h('div', { class: 'cc-loading-ball' }),
            h('div', { class: 'cc-loading-ball' }),
            h('div', { class: 'cc-loading-ball' }),
            h('div', { class: 'cc-loading-ball' })
          ])
      };
    }
  },
  dataHandler: Function,
  emptyImgOrStr: {
    type: String,
    default: config.emptyImgOrStr,
    validator(value: string): boolean {
      let valid: boolean = EmptyType.includes(value);
      if (!valid) {
        throw new Error(`size must be one of str / img`);
      }
      return valid;
    }
  },
  emptyText: {
    type: String,
    default: config.emptyText
  },
  emptyImgWidth: {
    type: Number,
    default: config.emptyImgWidth
  },
  emptyImgHeight: {
    type: Number,
    default: config.emptyImgHeight
  },
  emptyImgSrc: {
    tyep: String,
    default: config.emptyImgSrc
  },
  showContextmenu: Boolean
};
