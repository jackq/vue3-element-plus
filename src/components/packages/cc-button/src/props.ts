/*
 * @Description:
 * @Author: Devin-chen
 * @Date: 2021-10-25 22:25:36
 * @LastEditTime: 2022-04-14 17:12:38
 * @LastEditors: Devin
 * @Reference: 原创
 */

// 按钮的尺寸
const SIZELIST: Array<string> = ['large', 'default', 'small'];
import config from '@/utils/packages-configs/cc-button';

let props = {
  size: {
    type: String,
    default: config.size,
    validator(value: string): boolean {
      let valid: boolean = SIZELIST.includes(value);
      if (!valid) {
        throw new Error(`size must be one of large / default / small`);
      }
      return valid;
    }
  },
  type: {
    type: String,
    default: config.type
  },
  autoInsertSpace: {
    type: Boolean,
    default: config.autoInsertSpace
  },
  tag: String
};

export default props;
