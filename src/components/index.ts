/*
 * @Description: 
 * @Date: 2022-01-19 16:29:07
 * @LastEditTime: 2022-01-26 17:30:31
 * @FilePath: /vue3-element-plus/src/components/index.ts
 * @Author: Devin
 */
import Code from './code/index.vue';
export const Common = {
  install(Vue: any) {
    Vue.component('Code', Code);
  }
};