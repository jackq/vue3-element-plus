/*
 * @Description:
 * @Date: 2021-10-22 09:39:41
 * @LastEditTime: 2022-04-28 13:51:22
 * @FilePath: /vue3-element-plus/src/router/index.ts
 * @Author: Devin
 */
import Login from '@/views/login/index.vue';
import Layout from '@/layout/index.vue';

import { createRouter, createWebHashHistory,createWebHistory, RouteRecordRaw, Router } from 'vue-router';
import componentsRoutes from './components';
import other from './other';
const routes = [
  { path: '/', redirect: '/login', hidden: true },
  { path: '/login', name: 'login', component: Login, hidden: true },
  {
    path: '/home',
    name: 'home',
    redirect: '/home/index',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'HomeIndex',
        component: () => import('@/views/home/index.vue'),
        meta: {
          title: '首页',
          icon: 'icon-shouye'
        }
      }
    ]
  },
  {
    path: '/guide',
    name: 'Guide',
    component: Layout,
    redirect: '/guide/index',
    children: [
      {
        path: 'index',
        name: 'Guide',
        component: () => import('@/views/guide/index.vue'),
        meta: {
          title: '引导页',
          icon: 'icon-yindao'
        }
      }
    ]
  },
  componentsRoutes,
  other,
  {
    path: '/menu1',
    name: 'menu1',
    component: Layout,
    redirect: '/menu1/index',
    children: [
      {
        path: 'index',
        name: 'menu1',
        component: () => import('@/views/guide/index.vue'),
        meta: {
          title: '菜单一',
          icon: 'icon-yindao'
        }
      }
    ]
  },
  {
    path: '/menu2',
    name: 'menu2',
    component: Layout,
    redirect: '/menu2/index',
    children: [
      {
        path: 'index',
        name: 'menu2',
        component: () => import('@/views/guide/index.vue'),
        meta: {
          title: '菜单二',
          icon: 'icon-yindao'
        }
      }
    ]
  },
  {
    path: '/menu3',
    name: 'menu3',
    component: Layout,
    redirect: '/menu3/index',
    children: [
      {
        path: 'index',
        name: 'menu3',
        component: () => import('@/views/guide/index.vue'),
        meta: {
          title: '菜单三',
          icon: 'icon-yindao'
        }
      }
    ]
  },
  {
    path: '/menu4',
    name: 'menu4',
    component: Layout,
    redirect: '/menu4/index',
    children: [
      {
        path: 'index',
        name: 'menu4',
        component: () => import('@/views/guide/index.vue'),
        meta: {
          title: '菜单四',
          icon: 'icon-yindao'
        }
      }
    ]
  },
  {
    path: '/menu5',
    name: 'menu5',
    component: Layout,
    redirect: '/menu5/index',
    children: [
      {
        path: 'index',
        name: 'menu5',
        component: () => import('@/views/guide/index.vue'),
        meta: {
          title: '菜单五',
          icon: 'icon-yindao'
        }
      }
    ]
  },
];

const router: Router = createRouter({
  history: createWebHashHistory(),
  routes: routes
});

export default router;
